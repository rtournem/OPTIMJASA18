This repository is dedicated to the optimization of brass instruments
based on sound simulation using the harmonic balance technique.

This Readme explains the code structure and low level strategies used
by the optimization procedure developped during my Ph.D. thesis and
published in [this
article](https://mechanicaldesign.asmedigitalcollection.asme.org/article.aspx?articleid=2594950)
(Journal of Mechanical Design).  the code is in Matlab, and it uses
the [NOMAD](https://www.gerad.ca/nomad/) optimizer that you have to
install by yourself. The code has been mainly developped between 2015
and 2018, and this is a simplified version for distribution purposes,
if you want to use it under another lisence, contact me.

I give it freely with absolutely no guarantee of functionning. It uses
some advanced Matlab and Parallel Toolbox tricks. I think that if my
explanations are clear enough, you may need between 3 days and 1 week
to make it work completely.

# Processing

The entire procedure is divided into 2 steps:
1. a preprocessing of the design problem managed by the script
   `WORK/FREQUENTIAL_SIMULATION/batchEH.m`
2. the actual optimization process based on this preprocessing
   launched by the script
   `WORK/BORE_OPTIMIZATION/RunRaw/BlackBox/optim.sh`. Actually, I used
   an HPC environement managed by the SLURM scheduler. I left a SLURM
   batch to launch several optimization tasks:
   `WORK/BORE_OPTIMIZATION/RunRaw/BlackBox/jobSLURMEXAMPLE.sh`

Detailed explanations for the preprocessing and the optimization
process are present respectively in the folder
`WORK/FREQUENTIAL_SIMULATION` [link readme
preprocessing](WORK/FREQUENTIAL_SIMULATION) and
`WORK/BORE_OPTIMIZATION` [link readme
optimization](WORK/BORE_OPTIMIZATION).

# Data

* The folder `DATA/optim/Catalyst` contains the data files from the
  preprocessing I used to compute the optimization for the JASA
  article of 2019. You can use them to reproduce some results. The tag
  `MP2TMM` represents the 2D example of the article and the tag `LP10`
  the 10D example.
* Precomputed transfert matrices needed to reproduce the results are
  located in the folder `DATA/impedance/ZModelisationTournemenne`
* Preprocessing intermediate results are written in the folder
  `DATA/HarmBal/Results/Batch` 
  
In general, I am sorry for the folder structure. I tried to keep it
simple but I don't want to break too much the relative positions in
order to avoid path errors...
