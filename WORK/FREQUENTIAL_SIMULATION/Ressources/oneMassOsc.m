function Y=oneMassOsc(musicianNFO,simuNFO)
% one-mass oscillator of the lip-reed model.

Pi=simuNFO.Pi;
N=simuNFO.N;
%Initialisation parameters
wa = 2 * pi * musicianNFO.fa;
ga = wa/musicianNFO.Ql;   %damping factor
f = Pi(:,N+3);    
w = 2 * pi *f;   

%Real and imaginary parts of Cn, ReC et ImC (2N+3,N+1)
ReC=Pi(:,1:N+1);
ImC=Pi(:,N+2:2*N+2);
ImC(:,2)=0; 
ReC(:,1)=-(musicianNFO.pm-ReC(:,1));

Y=[]; % Y(2N+3,N+1)
for k=1:2*N+3
	for n=1:N+1
		Y(k,n)=(ReC(k,n) + 1i*ImC(k,n)) / (musicianNFO.mua*(wa^2 + 1i*(n-1)*w(k)*ga - ((n-1)*w(k))^2));
	end
end
