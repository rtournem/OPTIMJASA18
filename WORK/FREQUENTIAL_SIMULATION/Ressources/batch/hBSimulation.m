function [success,allNotes]=hBSimulation(pmmuafaxs,param,nfo,varargin)
%the function that actually does the simulation. You give an instrument, a set
%of musician parameters and it tries many initial vector for the harmonic
%balance technique and stores the simulated sounds.

%possibility to add sols in the initial sols vector
if (nargin>=4) SpecialSol=varargin{1}; else SpecialSol=[]; end
success=zeros(size(pmmuafaxs,1),1);
N=param.N;
%Notes contains on each line the mask,  the associated solution and the geometrical parameters.
allNotes=[];
%%                        BOUCLE  P=g(mask,Pi,Z)
for i=1:size(pmmuafaxs,1)	
	mask=pmmuafaxs(i,1:3);
	if mod(i,100) == 0
		disp(num2str(pmmuafaxs(i,:)));
	end
	if mod(i,10) == 0
		disp(i)
	end
	%%                          IMPEDANCE
	nfo.imped=impedPreProcess(2,param.type,pmmuafaxs(i,4:end),nfo.imped);
	try
		[nfo.Instru.imped,fPeaks,holes]=zCreation(nfo.imped);
	catch
		disp('this candidate did not work');
		continue;
	end
	%%                      VECTEUR INITIAL
	try 
		Pis=initVectHB(mask,pmmuafaxs(i,4:end),[SpecialSol; allNotes],fPeaks,holes,param);
	catch
		disp('Pis not created: PROBLEM');
		continue;
	end
	if ~isfield(param,'verb')%field verb set at about line 40 of Range2Notes.m
% 		disp([num2str(mask(1)) '_' num2str(mask(2)) '_' num2str(mask(3))]);
		disp(['The number of initial vectors is: ' num2str(size(Pis,1))]);
	end
	%%                      BOUCLE  P=h(Pis)
	for j=1:size(Pis,1)
		nfo.Simu.Pi=Pis(j,:);
		nfo.mask.pm=mask(1);
		nfo.mask.mua=mask(2);
		nfo.mask.fa=mask(3);
		try
			[IsGood,~,Pfinal,nfo.mask,nfo.Instru,nfo.Simu,nfo.Phys,nfo.Optim]=...
				eqHarmo(nfo.mask,nfo.Instru,nfo.Simu,nfo.Phys,nfo.Optim);
		catch
			IsGood=0;
		end
		if IsGood
			reg=find(holes<=Pfinal(N+3),1,'last'); %to see if I keep it
			if ~isfield(param,'verb')
			end
			success(i)=1;
			if param.RadXP
				Pext=stf(Pfinal,Pfinal(N+3),N,nfo.Instru.XpDir);
				Pfinal=[Pext(1:N+2) Pfinal(N+3) Pext(N+4:end)];
			end
			%avoiding to store twice the same sound
			if isempty(allNotes)
				allNotes=[allNotes; mask, Pfinal(N+3), Pfinal, reg, pmmuafaxs(i,4:end)];
				allNotes(end,N+7)=0;
			else
				%is it the first sound for this set of parameters?
				if isempty(find(ismember(allNotes(:,1:3),mask,'rows'), 1))
					allNotes=[allNotes; mask, Pfinal(N+3), Pfinal , reg, pmmuafaxs(i,4:end)];
					allNotes(end,N+7)=0;
				else
					%did this sound is the same the one found before?
					if isempty(find(abs(allNotes(ismember(allNotes(:,1:3),mask,'rows'),4)-Pfinal(N+3))<10^-3, 1))
						allNotes=[allNotes; mask, Pfinal(N+3), Pfinal , reg, pmmuafaxs(i,4:end)];
						allNotes(end,N+7)=0;
					end
				end
			end
			%when we have 2 solutions in the lowest regime (general case is
			%2 solution in the lowest regime and one in superior regime)
			%then we stop the search cause' we have all we need. (speed
			%objective).
			% edit Robin 12/03/18 taking advantages of the sequentiality of the
			% problem.
			lastNotes = (size(allNotes,1)-2 : size(allNotes,1));
			lastNotes(lastNotes<=0) = []; %if soundsGeom is too small
			lastNotes(~ismember(allNotes(end-(length(lastNotes)-1):end,1:3),mask,'rows')) = [];
			MusiFoundFreqs = allNotes(lastNotes,4);
			
			TargetReg=find(holes-min(MusiFoundFreqs)>0,1,'first')-1;
			if ~isempty(TargetReg)
				if length(find(fPeaks(TargetReg)-10<MusiFoundFreqs & MusiFoundFreqs<holes(TargetReg+1)))==2
					break;
				end
			end
		end
	end
end

end
