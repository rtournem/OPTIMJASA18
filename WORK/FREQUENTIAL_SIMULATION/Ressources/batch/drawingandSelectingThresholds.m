function [param, note, thresholds] = drawingandSelectingThresholds(param, note)
%manual tool for "power" thresholds selection. 

%% sounds visualization
figure
sh(1)=subplot(1,2,1);
scatter3(note.allStandardGeom(:,1),note.allStandardGeom(:,2),note.allStandardGeom(:,3),...
	10,note.allPow,'filled');
cameramenu;
sh(2)=subplot(1,2,2);
scatter3(note.allStandardGeom(:,1),note.allStandardGeom(:,2),note.allStandardGeom(:,3),...
	10,note.allFPlay,'filled');
cameramenu;
linkprop(sh,{'CameraPosition','CameraUpVector'});

%% sigma according to percentage
h=figure('Name','line rangePm, Col regime, content sigmaFplay / percentage');
g=figure('Name','line rangePm, Col regime, content sigmaFplay / threshold');
hh=figure('Name','line rangePm, Col regime, content sigmaFplay / threshold');
for idxpm = 1:size(param.Pm2keep, 1)
		for reg = param.Regimes
			figure(h);
			subplot(size(param.Pm2keep,1),max(param.Regimes),(idxpm-1)*max(param.Regimes)+reg);
			plot(note.percentages{idxpm,reg},note.sortedFPlaysVar{idxpm,reg},'*');
			figure(g);
			subplot(size(param.Pm2keep,1),max(param.Regimes),(idxpm-1)*max(param.Regimes)+reg);
			plot(note.sortedPow{idxpm,reg},note.sortedFPlaysVar{idxpm,reg},'*');
			figure(hh);
			subplot(size(param.Pm2keep,1),max(param.Regimes),(idxpm-1)*max(param.Regimes)+reg);
			plot(note.percentages{idxpm,reg},note.sortedPow{idxpm,reg},'*');		
		end
end

for idxpm = 1:size(param.Pm2keep, 1)
	for reg = param.Regimes
		%each percentage correspond to a threshold taken automatically
		CurPerc = input(['Chose percentage for the dynamics'...
			num2str(idxpm) ' at regime ' num2str(reg) ':  ']);
		idxLists = find(note.percentages{idxpm, reg} > CurPerc, 1, 'first');
		disp(['You take ' num2str(idxLists) ' simu on ' num2str(length(note.percentages{idxpm, reg})) ' available.']);	
		note.Thresholds(idxpm, reg) = note.sortedPow{idxpm, reg}(idxLists); 
		thresholds.percval(idxpm, reg) = CurPerc;
		
		disp(['The threshold value is ' num2str(note.Thresholds(idxpm,reg))]);
		%we take every note until the last good one.
		note.taken{idxpm} = [note.taken{idxpm}; note.sortedSounds{idxpm, reg}(1:idxLists,:)];
		note.powTaken{idxpm} = [note.powTaken{idxpm}; note.sortedPow{idxpm, reg}(1:idxLists)];
	end
	figure
	scatter3(note.taken{idxpm}(:,1), note.taken{idxpm}(:,2), note.taken{idxpm}(:,3),...
		10, note.taken{idxpm}(:,7 + 2 * param.N) , 'filled');
	cameramenu;
end
thresholds.raw.perc = note.percentages;
thresholds.raw.pow = note.sortedPow;
thresholds.raw.FPlaysVar = note.sortedFPlaysVar;
thresholds.pms = param.Pm2keep;
thresholds.reg = param.Regimes;
thresholds.val = note.Thresholds;
end
