function [note,param]=range2Notes(param, note, option, varargin)
%manage the simulations according to the parameters.

nfo.imped=impedPreProcess(1,param.type);%1 because it is the step one of the imped parameterization

[nfo,param]=nfoGeneralParam(nfo,param);%FUNCTION SETTING EVERY FIXED paramS
%%                   ORCHESTRA MATRIX
pmmuafaxs=maskGeomCreation(param,option);
disp('points to simulate created!')
%let us know which points works or not.
if option.Workers>1
	poolSimu=gcp('nocreate');%if no worker opened.
	if isempty(poolSimu)
		parpool(option.Workers);
	elseif poolSimu.NumWorkers~=option.Workers %if the number of opened worker is not right.
		delete(gcp('nocreate'))
		parpool(option.Workers);
	end
	param.verb=0;
	parfor i=1:option.Workers %divide to conquer
		[success{i},notes{i}]=hBSimulation(pmmuafaxs{i},param,nfo);
	end
	% 	delete(gcp('nocreate'))
	note.All=cat(1,notes{:});
	note.success=cat(1,success{:});
	param.All=cat(1,pmmuafaxs{:});
else
	if param.NbPts2Sim<100 %meaning that we are not in the parallelized refin
		delete(gcp('nocreate'))%just in case the old workers are still there.
	end
	[note.success,note.All]=hBSimulation(pmmuafaxs,param,nfo,varargin{:});
	param.All=pmmuafaxs;
end
end
