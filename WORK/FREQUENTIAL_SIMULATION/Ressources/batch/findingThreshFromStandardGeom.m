function [note, param] = findingThreshFromStandardGeom(param, option)
%function used to simulates many sounds for one standard geometry.
N=param.N;

%%  Latin hyper sampling
%sampling of the space pm, mua, fa. Every sample will be tested by the harmonic
%balance technique. 
pmmuafaReg=lhsdesign(param.NbPts2Sim4StandardGeometry,3);

pmmuafaReg(:,1)=min(param.Pm2keep(:))+(max(param.Pm2keep(:)) -...
	min(param.Pm2keep(:))).*pmmuafaReg(:,1);
pmmuafaReg(:,2)=param.muas(1)+(param.muas(2)-param.muas(1)).*pmmuafaReg(:,2);
pmmuafaReg(:,3)=param.fas(1)+(param.fas(2)-param.fas(1)).*pmmuafaReg(:,3);

pmmuafaxs=[pmmuafaReg repmat(param.xstandard,size(pmmuafaReg,1),1)];

%% Sound simulations
if ~isempty(strfind(param.ProtectionFile,'Protecting'))
	if isempty(strfind(param.ProtectionFile,'Cleaning'))
		load(param.ProtectionFile);
		
		%% Place for tiny settings linked to data formatting and post experiment modifications
		%especially for this big file...
		%ProtectingSaveFindingThreshold_2018_3_10_8_50_17
		param.ProtectionFile = [];
		disp('loaded');
		fPlay = soundsGeom(:,4);
		noteRegimes = soundsGeom(:,7+2*N);
		pow = soundsGeom(:,5:N+5)+1i*soundsGeom(:,N+6:2*N+6);
		pow = sqrt(sum(abs(pow).^2,2));
		pow=pow./soundsGeom(:,1);
	end
else
	if option.Workers>1
		SubBatch=floor(size(pmmuafaxs,1)/option.Workers);
		for i=1:option.Workers
			if i==option.Workers %to tackle the problem when the length is not a multiple.
				temp{i}=pmmuafaxs((i-1)*SubBatch+1:end,:);
			else
				temp{i}=pmmuafaxs((i-1)*SubBatch+1:i*SubBatch,:);
			end
		end
		pmmuafaxsSimu=temp;
	end
	% 1 because it is the step one of the imped parameterization
	nfo.imped=impedPreProcess(1,param.type);
	[nfo,param]=nfoGeneralParam(nfo,param);%FUNCTION SETTING EVERY FIXED PARAMS
	
	if option.Workers>1
		poolSimu=gcp('nocreate');%if no worker opened.
		if isempty(poolSimu)
			parpool(option.Workers);
		elseif poolSimu.NumWorkers~=option.Workers %if the number of opened worker is not right.
			delete(gcp('nocreate'))
			parpool(option.Workers);
		end
		param.verb=0;
		parfor i=1:option.Workers %divide to conquer
			Decomponotes{i}=hBSimulationOneGeom(pmmuafaxsSimu{i},param,nfo);
		end
		% 	delete(gcp('nocreate'))
		soundsGeom=cat(1,Decomponotes{:});
	else
		if param.NbPts2Sim<100 %meaning that we are not in the parallelized refin
			delete(gcp('nocreate'))%just in case the old workers are still there.
		end
		%creation of the three data variables: soundsGeom (the actual sound and from
		%it fPlay, pow, noteRegimes)
		soundsGeom=hBSimulationOneGeom(pmmuafaxs,param,nfo);
	end
	
	save([param.pathResults '/Protection/ProtectingSaveFindingThreshold' clockFormat],...
		'soundsGeom', 'param', 'option', 'pmmuafaxs');fPlay = soundsGeom(:,4);
	noteRegimes = soundsGeom(:,7+2*N);
	pow = soundsGeom(:,5:N+5)+1i*soundsGeom(:,N+6:2*N+6);
	pow = sqrt(sum(abs(pow).^2,2));
	pow=pow./soundsGeom(:,1);
end



%% cleaning solutions
if ~isempty(strfind(param.ProtectionFile,'Cleaning'))
	load(param.ProtectionFile);
	disp('loaded');
	%% Place for tiny settings linked to data formatting and post experiment modifications
	param.Regimes = 2:5;
else
	%I could make that fast: parrallelizing the thing writing in a index and at the
	%end supressing the right lines!
	pmmuafaSol=unique(soundsGeom(:,1:3),'rows');
	if option.Workers>1
		SubBatch=floor(size(pmmuafaSol,1)/option.Workers);
		for i=1:option.Workers
			if i==option.Workers %to tackle the problem when the length is not a multiple.
				temp{i}=pmmuafaSol((i-1)*SubBatch+1:end,:);
			else
				temp{i}=pmmuafaSol((i-1)*SubBatch+1:i*SubBatch,:);
			end
		end
		pmmuafaSol2Process=temp;
	end
	
	if option.Workers>1
		parfor j=1:option.Workers %divide to conquer
			idxBatch{j} = zeros(size(soundsGeom(:,1)));
			for i=1:size(pmmuafaSol2Process{j},1) %taking away the multisolutions
				idxInAllnotes = not(any(bsxfun(@minus,soundsGeom(:,1:3),...
					pmmuafaSol2Process{j}(i,:)),2)); %hyper rapide.
				if sum(idxInAllnotes)>=1
					[~,idxBestnote]=max(pow(idxInAllnotes));%the note with the highest power
					idxActiv=find(idxInAllnotes);
					idxInAllnotes(idxActiv(idxBestnote))=0;
				end
				idxBatch{j} = idxBatch{j} + idxInAllnotes;
			end
		end
		idxBatchs = cell2mat(idxBatch);
		idxBatchs = sum(idxBatchs,2);
		idxBatchs = logical(idxBatchs);
		if sum(idxBatchs)>=1
			soundsGeom(idxBatchs,:)=[];%removing the line
			pow(idxBatchs)=[];
			fPlay(idxBatchs)=[];
			noteRegimes(idxBatchs)=[];
		end
	else
		%taking away the multisolutions (the multicore solution is 10 times faster...
		%if it works consistently, I will implement it also in the non multi-core
		%case).
		for i=1:size(pmmuafaSol,1)
			idxBatch = zeros(size(soundsGeom(:,1)));
			idxInAllnotes=not(any(bsxfun(@minus,soundsGeom(:,1:3),pmmuafaSol(i,:)),2)); %hyper rapide.
			if sum(idxInAllnotes)>=1
				[~,idxBestnote]=max(pow(idxInAllnotes));%the note with the highest power
				idxActiv=find(idxInAllnotes);
				idxInAllnotes(idxActiv(idxBestnote))=0;
				soundsGeom(idxInAllnotes,:)=[];%removing the line
				pow(idxInAllnotes)=[];
				fPlay(idxInAllnotes)=[];
				noteRegimes(idxInAllnotes)=[];
			end
		end
	end
	save([param.pathResults '/Protection/ProtectingFindThreshPostCleaning' clockFormat],...
		'soundsGeom', 'param', 'option', 'pmmuafaxs', 'pow', 'fPlay', 'noteRegimes');
end

for idxpm = 1:size(param.Pm2keep,1)
	note.taken{idxpm} = [];
	note.powTaken{idxpm} = [];
	soundsGeomCurrentPm = soundsGeom(soundsGeom(:,1) > param.Pm2keep(idxpm,1) & soundsGeom(:,1)...
		< param.Pm2keep(idxpm,2),:);
	powCurrentPm = pow(soundsGeom(:,1) > param.Pm2keep(idxpm,1) & soundsGeom(:,1)...
		< param.Pm2keep(idxpm,2));
	fPlayCurrentPm =  fPlay(soundsGeom(:,1) > param.Pm2keep(idxpm,1) & soundsGeom(:,1)...
		< param.Pm2keep(idxpm,2));
	noteRegimesCurrentPm = noteRegimes(soundsGeom(:,1) > param.Pm2keep(idxpm,1) & soundsGeom(:,1)...
		< param.Pm2keep(idxpm,2));
	for reg = param.Regimes
		%arbitrary ratio under which we tell that there is a problem.
		if sum(noteRegimesCurrentPm == reg) < length(noteRegimesCurrentPm) /length(param.Regimes)/15
			error(['not enough points (' num2str(sum(noteRegimesCurrentPm == reg)) 'points)'...
				'for the dynamics' num2str(param.Pm2keep(idxpm,1))...
				' - ' num2str(param.Pm2keep(idxpm,2)) ', at regime ' num2str(reg)]);
		end
		% for variable name size reason, even if only Reg is added at the end of the
		% file name they also depend on the idxpm
		soundsGeomReg = soundsGeomCurrentPm(noteRegimesCurrentPm == reg,:);
		powCurrentReg = powCurrentPm(noteRegimesCurrentPm == reg);
		fPlayCurrentReg = fPlayCurrentPm(noteRegimesCurrentPm == reg);
		[sortedPow, idxSortedPow] = sort(powCurrentReg, 'descend');
		sortedFPlays = fPlayCurrentReg(idxSortedPow);
		sortedSoundsGeomReg = soundsGeomReg(idxSortedPow,:);
		cumcount = 1:length(sortedFPlays);
		cumcount = cumcount.';
		
		cummean = bsxfun(@rdivide, cumsum(sortedFPlays), cumcount);
		sortedFPlaysVar = sqrt(bsxfun(@rdivide, cumsum(sortedFPlays.^2), cumcount) -cummean.^2);
		percCurrentRegNote=(1:length(powCurrentReg))*100/length(powCurrentReg);
		
		%thoughts about my final choice regarding the "power" threshold selection:
		%we had before an automatic threshold selection here based on player recordings on a
		%real trumpet. Unfortunately, it may be better 
		%to set it arbitrarily for two reasons:
		%1st, the recording don't represent the musican variability over the trumpet
		%because it is impossible to know at which level the musicians used their
		%unconscious knowledge of music... In other terms, we don't know how
		%musicians have been influenced by their usual practise in the making of
		%these sounds.
		%2nd the playing frequency variation for musician is giving interesting
		%hints regarding the valid threshold to consider. Yet, it gives us only
		%information that we have to balance with the % of masks discarded while
		%considering the corresponding threshold. We don't want to be too harsh on
		%the threshold because it will eventually reduce the design space removing
		%maybe very interesting instrument according some acoustic descriptors.
		%Finally, a non-polemic argument is that simulations are different from
		%experience. This is especially true in our case considering the fact that
		%our model is simple, and consequently imperfect.
		%another remark is the fact that this selection strategy based on the
		%threshold is limited, it sounds valid on a physical point of view, but no
		%measurements on actual musician as been made. Yet, it is very well
		%correlated to the distance to impedance peak, which tells us that in the 
		%general case the better the musician the higher this threshold. 
		
		note.percentages{idxpm, reg} = percCurrentRegNote;
		note.sortedFPlays{idxpm, reg} = sortedFPlays;
		note.sortedFPlaysVar{idxpm, reg} = sortedFPlaysVar;
		note.sortedPow{idxpm, reg} = sortedPow;
		note.sortedSounds{idxpm, reg} = sortedSoundsGeomReg;
	end
end
note.allStandardGeom = soundsGeom;
note.allPow = pow;
note.allFPlay = fPlay;
end
