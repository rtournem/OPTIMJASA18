function [impedNFO,x,y,borders,xsamp,ysamp]=impedPreProcess(varargin)
%this file let you manage the input impedance of each design problem!
%if you define a new design problem, you will have to modify steop 1 and 2 of
%this file. 

%the input impedance has to be dimensionned (having units: values around 1e8 and
%not 1) 

step=varargin{1};
impedtype=varargin{2};
global ROOT
if isempty(ROOT)
	ROOT='../../'; %FOR THE PARALLEL LOOP... ONLY SOLUTION
end
if step==1
	disp(impedtype);
	if strcmp(impedtype, 'MP2TMM')
		impedNFO.toolboxModel = 'Tournemenne';
		load([ROOT 'DATA/impedance/ZModelisationTournemenne/MP2']);
		impedNFO.instru = instru;
	elseif strcmp(impedtype, 'LP10')
		impedNFO.toolboxModel = 'Tournemenne';
		load([ROOT 'DATA/impedance/ZModelisationTournemenne/LP10']);
		impedNFO.instru = instru;
		impedNFO.TMMs = Mguides;
	end
elseif step==2
	x=varargin{3};
	impedNFO=varargin{4};
	if strfind(impedtype,'MP2TMM')
		impedNFO.geom = impedNFO.instru;
		impedNFO.geom(1).longueur = x(1);
		impedNFO.geom(4).rayon(2) = x(2);
		impedNFO.geom(5).rayon = x(2);
		impedNFO.geom(6).rayon(1) = x(2);
	elseif strfind(impedtype,'LP10')
		impedNFO.geom = impedNFO.instru;
		for i = 1 : length(x)
			impedNFO.geom(1+i).rayon(2) = x(i);
			impedNFO.geom(2+i).rayon(1) = x(i);			
		end
	end
elseif step==3 %testoptprob
	x=varargin{3};
	impedNFO=0; %USELESS
	if strcmp(impedtype,'MP3Braden')
		lgthElts=[0.00133457944, 0.00164859813, 0.00125607477, 0.00062803738, 0.00141308411,...
			0.00117757009, 0.0017271028, 0.00408224299, x(2), 0.08-x(2), 0.055, 0.055, 0.055, 0.055,...
			0.495, 0.02, 0.019, 0.03, 0.035,0.03, 0.029, 0.03, 0.031, 0.03, 0.061, 0.03, 0.031, ...
			0.032, 0.03, 0.025, 0.036, 0.03, 0.034, 0.029, 0.028 ];
		radiiElts={[0.0084, 0.00800747664],[0.00800747664, 0.00730093458],[0.00730093458, 0.00604485981],...
			[0.00604485981, 0.00533831776],[0.00533831776, 0.00368971963],[0.00368971963, 0.00298317757],...
			[0.00298317757, 0.00235514019],[0.00235514019, 0.001825],x(3),[x(3), 0.00464],[0.00464, 0.005],...
			[0.005, 0.0055],[0.0055,0.0057],[0.0057,0.005825],0.005825,[0.005825, 0.006125],[0.006125, 0.0061],...
			[0.0061, 0.005985],[0.005985, 0.006205],[0.006205, 0.006325],[0.006325, 0.00665],[0.00665, 0.00697],...
			[0.00697, 0.00739],[0.00739, 0.00845],[0.00845, 0.00868],[0.00868, 0.00925],[0.00925, 0.009845],...
			[0.009845, 0.010645],[0.010645, 0.01155],[0.01155, 0.01275],[0.01275, 0.014375],[0.014375, 0.0167],...
			[0.0167, 0.0225],[0.0225, 0.0347],[0.0347, 0.0610]};
		for i=1:length(radiiElts)
			ysamp(i)=radiiElts{i}(1);
		end
		ysamp(end+1)=radiiElts{end}(end);
		
		%scaling transformation
		longueur_emb=lgthElts(1:8);
		abscisse_emb=cumsum(longueur_emb);
		abscisse_emb_homo=abscisse_emb.*(x(1)/abscisse_emb(end));
		longueur_emb_homo=diff(abscisse_emb_homo);
		longueur_emb_homo=[longueur_emb(1),longueur_emb_homo];
		lgthElts=[longueur_emb_homo,lgthElts(9:end)];
		borders=[0 0.09];
	end
	lgthCum=cumsum(lgthElts);
	x=0:lgthCum(end)/100000:lgthCum(end);
	xsamp=[0,lgthCum];
	y=percefromX(x,xsamp,radiiElts);
end
end
function y=percefromX(x,xsamp,radiiElts)
y=[];
for i=1:length(xsamp)-1
	if i==length(xsamp)-1
		xelt=x(x>=xsamp(i) & x<=xsamp(i+1));
	else
		xelt=x(x>=xsamp(i) & x<xsamp(i+1));
	end
	yelt=(radiiElts{i}(end)-radiiElts{i}(1)).*(xelt-xsamp(i))./(xsamp(i+1)-xsamp(i))+radiiElts{i}(1);
	y=[y,yelt];
end
end























