function optimCatalystCreation(param,note)
%file creation for the optimization procedure. they help the optimization, so I
%called them catalyst files.

global ROOT
toWrite=0;
PathExport=[ROOT 'DATA/optim/Catalyst/'];
if exist([PathExport 'INITSOL/InitSol_' param.SetName '.mat'],'file')
	choice=input([param.SetName ' is already existing. Overwright? [y,n]'],'s');
	if strcmp(choice,'y') || strcmp(choice,'Y') || strcmp(choice,'Yes') || strcmp(choice,'yes')
		toWrite=1;
	end
else
	toWrite=1;
end
if toWrite
	neighborIdx = param.neighbor;
	InitSols=[];
	Markers=[];
	for idxpm=1:size(param.Pm2keep,1)
		idx2Keep=[];
		InitSolsPm=note.taken{idxpm};
		for reg=param.Regimes
			idx2KeepTemp=find(InitSolsPm(:,end-size(param.xs,1))==reg);
			idx2Keep(end+1:end+length(idx2KeepTemp))=idx2KeepTemp;
		end
		InitSolsPm=InitSolsPm(idx2Keep,:);
		MarkersPm=(idxpm-1)*10+InitSolsPm(:,end-size(param.xs,1));
		InitSols=[InitSols ; InitSolsPm];
		Markers=[Markers; MarkersPm];
		
		Pms{idxpm}=param.Pm2keep(idxpm,:);
		Thresholds{idxpm}=note.Thresholds(idxpm,:);
		neighborsEntire{idxpm} = note.entirePercentiles(idxpm, :);
	  neighborsDesign{idxpm} = note.designPercentiles(idxpm, :);
		%cell-array en idxpm et reg
		minVal = note.minVal;
		maxVal = note.maxVal;
	end
	save([PathExport 'INITSOL/InitSol_' param.SetName],'InitSols','Markers');
	save([PathExport 'THRESHOLDS/Thresholds_' param.SetName],'Thresholds', 'Pms', 'neighborsEntire', 'neighborsDesign', 'neighborIdx', 'minVal', 'maxVal');
end
end
