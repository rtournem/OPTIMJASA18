function [fplays,pows,Spectrums,fPeaks,holes,reg6,impossibleGeom]=...
	simpleSimus(param,masks,fas,Cdts,nfo)
%simulations for the test of the design problem

impossibleGeom=[];
N=param.N;
k=1;
for Cdt=Cdts'
	disp(num2str(Cdt'));
	nfo.imped=impedPreProcess(2,param.type,Cdt,nfo.imped);
	try
		[imped,fPeaks{k},holes{k}]=zCreation(nfo.imped);
	catch
		impossibleGeom=[impossibleGeom;Cdt];
		continue;
	end
	nfo.Instru.imped=imped;
	
	%plotting the impedance
	if 1 %to set up accordingly
		gg=figure;
		plot(imped(:,1),abs(imped(:,2)+1i*imped(:,3)),'r');
		hold on;
		for i=1:length(fPeaks{k})
			plot([fPeaks{k}(i) fPeaks{k}(i)],[0 max(abs(imped(:,2)+1i*imped(:,3)))],'b');
		end
		for i=1:length(holes{k})
			plot([holes{k}(i) holes{k}(i)],[0 max(abs(imped(:,2)+1i*imped(:,3)))],'g');
		end
		xlim([0 2000]);
		drawnow;
	end
	
	idxl=1;
	for l=masks'
		mask(1)=l(1);
		mask(2)=l(2);
		
		vectSize=ceil(length(fas)/nfo.Simu.Para);
		groupNb = nfo.Simu.Para;
		if nfo.Simu.Para > length(fas) %it means that I am not parallelizing the fas calculation
			groupNb = 1;
			vectSize = length(fas);
		end
		for kk=1:groupNb
			if (kk)*vectSize>length(fas)
				fasSplit{kk}=fas(1+(kk-1)*vectSize:end);
			else
				fasSplit{kk}=fas(1+(kk-1)*vectSize:(kk)*vectSize);
			end
		end
		if groupNb == 1
			for kk=1:groupNb
				[fplaysSplit{kk},powsSplit{kk},SpectrumsSplit{kk},reg6Split{kk}]=simpleFasSimus(fPeaks,holes,fasSplit{kk},mask,nfo,N,k);
			end
		else
			parfor kk=1:groupNb
				[fplaysSplit{kk},powsSplit{kk},SpectrumsSplit{kk},reg6Split{kk}]=simpleFasSimus(fPeaks,holes,fasSplit{kk},mask,nfo,N,k);
			end
		end
		reg6M{idxl}=max(cell2mat(reg6Split));
		fplaysM{idxl}=cat(1,fplaysSplit{:});
		powsM{idxl}=cat(1,powsSplit{:});
		SpectrumsM{idxl}=cat(1,SpectrumsSplit{:});
		
		idxl=idxl+1;
	end
	reg6{k}=max(cell2mat(reg6M));
	fplays{k}=fplaysM;
	pows{k}=powsM;
	Spectrums{k}=SpectrumsM;
	k=k+1;
end
end
function [fplaysM,powsM,SpectrumsM,reg6]=simpleFasSimus(fPeaks,holes,fas,mask,nfo,N,k)
idx=1;
disp(mask);
reg6=0;
fplaysM=zeros(length(fas),40);%we allow 40 different convergences.
powsM=zeros(length(fas),40);
SpectrumsM=zeros(length(fas),2*(N+1));
for i=fas
	mask(3)=i;
	%Pis!
	idxReg=[];
	if ~isempty(strfind(nfo.Simu.strat{1},'inf'))
		idxReg=[idxReg find(fPeaks{k}<mask(3),nfo.Simu.strat{2},'first')];
	end
	if ~isempty(strfind(nfo.Simu.strat{1},'sup'))
		idxReg=[idxReg find(fPeaks{k}>mask(3),nfo.Simu.strat{2},'first')];
	end
	if isempty(idxReg)
		continue;
	end

	reg=1;
	for h=idxReg
		idxendReg(reg)=find(holes{k}>fPeaks{k}(h),1,'first');
		reg=reg+1;
	end
	if strcmp(nfo.Simu.strat{3},'highBand')
		% 		WatchedRegimes=[fPeaks{k}(idxReg)'-10 holes{k}(idxendReg)' ];
		WatchedRegimes=[fPeaks{k}(idxReg)' holes{k}(idxendReg)'-5];%just to test (more little)

	elseif strcmp(nfo.Simu.strat{3},'largeBand')
		WatchedRegimes=[holes{k}(idxendReg-1)' holes{k}(idxendReg)' ];
	elseif strcmp(nfo.Simu.strat{3},'lowBand')
		WatchedRegimes=[holes{k}(idxendReg-1)' fPeaks{k}(idxReg)'+10  ];
	end
	Finits=[];
	for ii=1:size(WatchedRegimes,1)
		Finits=[Finits WatchedRegimes(ii,1):nfo.Simu.strat{4}:WatchedRegimes(ii,2)];
	end
% 	Finits=360:nfo.Simu.strat{4}:380;
	Pis=zeros(length(Finits),2*N+2);
	%Adding some possible final frequencies to the signal
	Pis(:,N+3)=Finits;
	%putting some energy on the first harmonic
	Pis(:,2)=mask(1)/2;
% 	ratio=-1/10;
% 	Pis(:,3)=Pis(:,2).*ratio;
	
  	disp(mask(3));
 	disp(size(Pis,1));
	idxPi=1;
	for j=1:size(Pis,1)
		nfo.Simu.Pi=Pis(j,:);
		nfo.mask.pm=mask(1);
		nfo.mask.mua=mask(2);
		nfo.mask.fa=mask(3);
		[isGood,~,Pfinal,nfo.mask,nfo.Instru,nfo.Simu,nfo.Phys,nfo.Optim]=...
			eqHarmo(nfo.mask,nfo.Instru,nfo.Simu,nfo.Phys,nfo.Optim);
		if isGood
			if Pfinal(N+3)>fPeaks{k}(6)
				reg6=1;

			end
			if isempty(find(fplaysM<=Pfinal(N+3)+10^(-3) & fplaysM >= Pfinal(N+3)-10^(-3), 1))
				fplaysM(idx,idxPi)=Pfinal(N+3);
				Pfinal(N+3)=0;
				pow=Pfinal(1:N+1)+1i*Pfinal(N+2:end);
				pow=sqrt(sum(abs(pow).^2,2));
				pow=pow./mask(1);
				powsM(idx,idxPi)=pow;
				if idxPi==1 
					SpectrumsM(idx,:)=Pfinal;
				end
				idxPi=idxPi+1;
			end
		end
		
	end
	idx=idx+1;
end
end
