function testSimuAna(param, testResults, option)
%this file draws the results of every tested geometry

global ROOT

%if you want to launch this file per se
if ~exist('param','var')
	ROOT='../../../../';
	path(path,genpath([ROOT 'WORK/IMPEDANCE']));
	close all
	load('/Users/robintournemenne/REPO/Matlab/DATA/HarmBal/Results/Batch/Tests/Var4LP1MP_2016_1_22_21_13_14');
	Candidates={0.004,0.004:0.001:0.006,0.004:0.001:0.006,0.004:0.001:0.006,0.004:0.001:0.006};
	option.morePlots=1;
end

for kk=1:length(testResults.fplaysAll{1})%for every mask
	CdtFail{kk}=[];
end
N=param.N;
for j=1:length(testResults.fplaysAll)%every candidate
	fplaysCdt=testResults.fplaysAll{j};
	PowsCdt=testResults.PowsAll{j};
	fPCdt=testResults.fPeaksAll{j};
	hCdt=testResults.holesAll{j};
	holesCdt=testResults.holesAll{j};
	if option.morePlots
		hh=figure('Name',[ num2str(param.TestCdts(j,:)) ' - ' num2str(param.TestMasks(kk,:)) ]);
		linenb=floor(sqrt(length(fplaysCdt)));
	end
	for kk=1:length(fplaysCdt)%for every mask
		%validity of the design (for this embouchure)
		notValid=0;
		for Reg=param.Regimes
			%find successful notes
			idxReg= fplaysCdt{kk}>holesCdt(Reg) & fplaysCdt{kk}<holesCdt(Reg+1);
			fastep=param.Testfas(2)-param.Testfas(1);
			if sum(idxReg(:))<(1/3*30/fastep)   %30/fastep is the arbitrary classical proportion of points we are expecting for each regime.		end
				notValid=notValid+1;
				if notValid==1
					disp(['design ' num2str(param.TestCdts(j,:)) ' on Musician ' num2str(param.TestMasks(kk,:)) ' is not valid']);
				end
				disp(['it lacks points in the ' num2str(Reg) ' regime : ' num2str(sum(idxReg(:))) ' points found for ' num2str(1/3*30/fastep) ' expected']);
			elseif notValid
				disp(['Regime ' num2str(Reg) ' is good with ' num2str(sum(idxReg(:))) ' points found']);
			end
		end
		if notValid
			CdtFail{kk}=[CdtFail{kk};param.TestCdts(j,:)]; %we add the wrong vector to the list of fail to show (then a kmeans clustering will show the type of bad impeds.
		end
		
		%optinal drawing of the musician convergence result
		if option.morePlots
			figure(hh);
			subplot(linenb,ceil(length(fplaysCdt)/linenb),kk);
			for i=1:size(fplaysCdt{kk},2)
				hold on;
				scatter(param.Testfas,fplaysCdt{kk}(:,i),20,PowsCdt{kk}(:,i),'filled');
			end
			for i=1:7 %regmax
				plot([param.Testfas(1) param.Testfas(end)],[fPCdt(i) fPCdt(i)],'b');
				plot([param.Testfas(1) param.Testfas(end)],[hCdt(i) hCdt(i)],'g');
				plot([fPCdt(i) fPCdt(i)],[0 hCdt(7)],'m');
			end
		end
	end
end

try
	for kk=1:length(fplaysCdt) %plotting the families of failed geometries (with a Kmeans you decided the number of class)
		CdtFailNorm = (CdtFail{kk} - repmat(min(CdtFail{kk}),size(CdtFail{kk},1),1)) ./ repmat(max(CdtFail{kk})-min(CdtFail{kk}),size(CdtFail{kk},1),1 );
		for i=2:min(30,size(CdtFail{kk},1)) %30 class maximum if possible
			warning('off','stats:kmeans:FailedToConvergeRep');
			[~,~,sumd]=kmeans(CdtFailNorm,i,...
				'replicates',100,'MaxIter',100,'Display','off');
			WSS(i)=sum(sumd);
		end
		figure;
		plot(WSS);
		drawnow;
		WSSInput=input('according to you, where is the elbow? (give the number of class you want) : ');
		idxClust=kmeans(CdtFailNorm,WSSInput,...
			'replicates',100,'MaxIter',100,'Display','off');
		for i=1:WSSInput
			figure;
			hold on;
			idxCurFail=find(idxClust==i);
			for j=1:length(idxCurFail)
				[~,x,y,borders]=impedPreProcess(3,param.type,CdtFail{kk}(idxCurFail(j),:));
				plot(x,y,x,-y,'Color',[1-j/length(idxCurFail) j/length(idxCurFail) 0]);
				xlim(borders);
			end
		end
	end
catch
	warning('cannot compute families of failed geometries');
end
end
