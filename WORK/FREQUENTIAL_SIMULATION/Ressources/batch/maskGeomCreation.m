function pmmuafaxs=maskGeomCreation(param,option)
%hyperlatin sampling providing a cloud of point in the entire parameter space
%(instrument  + action of the musician)

points2Sample=param.NbPts2Sim;
pms=[min(param.Pm2keep(:)), max(param.Pm2keep(:))];
muas=param.muas;
fas=param.fas;
xs=param.xs;

%preparing the batches for the parallel pools.
if option.Workers>1
	subBatch=floor(param.NbPts2Sim/option.Workers);
	parfor i=1:option.Workers
		pmmuafaxs=lhsdesign(subBatch,3+size(xs,1));
		pmmuafaxs(:,1)=pms(1)+(pms(2)-pms(1)).*pmmuafaxs(:,1);
		pmmuafaxs(:,2)=muas(1)+(muas(2)-muas(1)).*pmmuafaxs(:,2);
		pmmuafaxs(:,3)=fas(1)+(fas(2)-fas(1)).*pmmuafaxs(:,3);
		for j=1:size(xs,1)
			pmmuafaxs(:,3+j)=xs(j,1)+(xs(j,2)-xs(j,1)).*pmmuafaxs(:,3+j);
		end
		temp{i}=pmmuafaxs;
	end
	pmmuafaxs=temp;
else
	pmmuafaxs=lhsdesign(points2Sample,3+size(xs,1));
	pmmuafaxs(:,1)=pms(1)+(pms(2)-pms(1)).*pmmuafaxs(:,1);
	pmmuafaxs(:,2)=muas(1)+(muas(2)-muas(1)).*pmmuafaxs(:,2);
	pmmuafaxs(:,3)=fas(1)+(fas(2)-fas(1)).*pmmuafaxs(:,3);
	for i=1:size(xs,1)
		pmmuafaxs(:,3+i)=xs(i,1)+(xs(i,2)-xs(i,1)).*pmmuafaxs(:,3+i);
	end
end
end
