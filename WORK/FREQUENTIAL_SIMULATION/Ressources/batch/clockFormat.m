function clkstr=clockFormat(special)
clkstr=0;
clkarr=clock;
seconds=num2str(clkarr(6),2);
if clkarr(6)<10
	seconds=seconds(1);
end

if ~exist('special','var')
clkstr=['_' num2str(clkarr(1)) '_' num2str(clkarr(2)) '_' num2str(clkarr(3)) '_' num2str(clkarr(4)) '_' num2str(clkarr(5)) '_' seconds];
elseif strcmp(special,'noseconds')
	clkstr=['_' num2str(clkarr(1)) '_' num2str(clkarr(2)) '_' num2str(clkarr(3)) '_' num2str(clkarr(4)) '_'  num2str(clkarr(5))];
elseif strcmp(special,'nominutes')
	clkstr=['_' num2str(clkarr(1)) '_' num2str(clkarr(2)) '_' num2str(clkarr(3)) '_' num2str(clkarr(4))];
elseif strcmp(special,'nohours')
	clkstr=['_' num2str(clkarr(1)) '_' num2str(clkarr(2))  '_' num2str(clkarr(3))];
end
end