function [testResults, param] = testProbConv(param,option)
%function managing the test step of the preprocessing

if ~exist('param','var')%this if defines the standalone version of the function 
	path(path,genpath('../Ressources'));
	param.type='MP2TMM';% MP2TMM Mouthpiece_D0 MPModel LeadPipeMPDepth Var2LeadPipe
	param.N=6;
	param.TestCdts=[0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464 0.00464;
		0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825 0.005825;
		0.00464 0.00464 0.00464 0.00464 0.00464 0.005825 0.005825 0.005825 0.005825 0.005825;
		0.005825 0.005825 0.005825 0.005825 0.005825 0.00464 0.00464 0.00464 0.00464 0.00464;
		0.0047 0.0048 0.0049 0.0050 0.0051 0.00522 0.00534 0.00546 0.00558 0.0057;
		0.0057 0.0056 0.0055 0.0053 0.0052 0.0051 0.0050 0.0049 0.0048 0.0047;
		0.0047 0.0050 0.0054 0.0057 0.006 0.006 0.0057 0.0054 0.0050 0.0047;];
	param.TestMasks=[5000 -1.5; 10000 -1];
	param.Regimes=3:5;%ce champ l? ou Testfas me semble de trop ? premi?re vue...
	% 	param.Testfas=310:5:360;
	param.Testfas=200:3:400;
	option.Workers=1;
	option.morePlots=1;
end

global ROOT 
% ROOT='../../';

N=param.N;
%I left this parameter because contrary to HBSimulation it has the 'sup'
%/'inf' option to make broader tests!
nfo.Simu.strat={'sup',2,'highBand',2};
%1: 'sup' 'inf' 'inf-sup' -> are you looking for higher or lower regimes ? (compared to flip)
%2: how many regimes higher (or lower)? ;
%3: 'highBand' 'lowBand'
%'largeBand' telling where you want to look arrond the peak ;
%4: the playing frequency precision of the initial solution scattering
nfo.imped=impedPreProcess(1,param.type);%1 because it is the step one of the imped parameterization
% nfo.imped(:,3)=-nfo.imped(:,3);

nfo=nfoGeneralParam(nfo,param); %FUNCTION SETTING EVERY FIXED paramS

CdtsAll=param.TestCdts;
masks=param.TestMasks;
fas=param.Testfas;

vectSize=ceil(size(CdtsAll,1)/option.Workers);
%the following lines helps to manage the 2 parfor strategies
%Indeed, it eliminates the risk of having a pool of 0 candidate for some cores!
%This situation can happen only when parallelizing fas in the SimpleSimus.m file.
groupNb = option.Workers;
if option.Workers > size(CdtsAll,1) %it means that I am not parallelizing the candidate calculation
	groupNb = 1;
	vectSize = size(CdtsAll,1);
end
for k=1:groupNb
	%the last core will get the remaining candidate
	%it is the suboptimal case: Candidates%cores is different from 0
	if (k)*vectSize>size(CdtsAll,1)
		Cdts{k}=CdtsAll(1+(k-1)*vectSize:end,:);
	else
		Cdts{k}=CdtsAll(1+(k-1)*vectSize:(k)*vectSize,:);
	end
end

nfo.Simu.Para=option.Workers;
for k=1:groupNb %can be parfored
	[fplays{k},Pows{k},Spectrums{k},fPeaks{k},holes{k},reg6{k},impossibleGeom{k}]=...
		simpleSimus(param,masks,fas,Cdts{k},nfo);
end
testResults.reg6All=cat(2,reg6{:});
testResults.fplaysAll=cat(2,fplays{:});
testResults.PowsAll=cat(2,Pows{:});
testResults.SpectrumsAll=cat(2,Spectrums{:});
testResults.fPeaksAll=cat(2,fPeaks{:});
testResults.holesAll=cat(2,holes{:});
end
