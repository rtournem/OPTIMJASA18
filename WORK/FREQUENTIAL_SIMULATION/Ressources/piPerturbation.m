function Pi=piPerturbation(OptimNFO,Pi,N)
%solution vector perturbation for the computation of the jacobian

Dp=OptimNFO.delp.*eye(2*N+2,2*N+2);
for j=1:2*N+2
	Dp(j,:)=Pi(1,:)+Dp(j,:);		
Pi(2:(2*N+3),:)=Dp;

end
