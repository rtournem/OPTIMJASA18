function U=temp2ProcessedFourier(u,simuNFO)
%clever exact computation of the fourrier coefficients from the temporal
%equations.

Nn=simuNFO.Nn;
N=simuNFO.N;

TFDU=[];
for ii=1:(2*N+3)
 TFDU(ii,:)=fft(u(ii,:),Nn);		% (2N+3,Nn)
end

U=[];
for j=1:(N+1)
  U(:,j)=real(TFDU(:,j))./Nn;
  U(:,j+N+1)=imag(TFDU(:,j))./Nn;		% (2N+3,2N+2)
end


end
