function u=nlFluidEq(y,p,musicianNFO,simuNFO,physNFO)          
%non-linear equation in the excitator

pm=musicianNFO.pm;
b=musicianNFO.b;
H0=musicianNFO.H0;
ro=physNFO.ro;

q=1.0 ;
u=[];
for n=1:2*simuNFO.N+3
	for i=1:size(p,2)
		if y(n,i)>-H0
			u(n,i)=(b*H0)*((1+y(n,i)/H0)^q)*sign(pm-p(n,i))*sqrt(2*abs(pm-p(n,i))/ro);
		else
			u(n,i)=0;
			y(n,i)=-H0;
		end
	end
end
