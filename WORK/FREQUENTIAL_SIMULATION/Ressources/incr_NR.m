function [Pi,FREQ,XJg]=incr_NR(Pi,Pf,N,delp)	
%computation of the Jacobian for the Newton-Raphson method. See the section
%2.2.5 of my Ph.D. thesis to undertand the calculation (p 30 exactly)

% Initialisation
Jf=[];
Jg=[];
Jfreq=[];
XJg=[];

for i=1:2*N+2
   for j=1:2*N+2
     Jf(i,j)=(Pf(j+1,i)-Pf(1,i))./delp;
   end
end

FREQ(1)=Pi(1,N+3);
Pi(:,N+3)=zeros(2*N+3,1);
Pi(N+4,N+3)=delp;

Jfreq(1)=0;
Jfreq(N+2)=0;

for i=2:N+1
  Jfreq(i)=(Pf(N+4,i)-Pf(1,i))/(delp*(i-1));
  Jfreq(i+N+1)=(Pf(N+4,i+N+1)-Pf(1,i+N+1))/(delp*(i-1));
end

for i=1:2*N+2
    for j=1:2*N+2
	   if i==j
		  if j~=2
			 Jg(i,j)=(1-Jf(i,j));
			 XJg(i,j)=-Jg(i,j);
		  else
			 Jg(i,j)=(1-Jf(i,j))+(Pf(1,i)-Pi(1,i))./(Pi(1,2));
			 XJg(i,j)=-Jg(i,j);
		  end
	   end
	   if i~=j
		  if j~=2
			 Jg(i,j)=-Jf(i,j);
			 XJg(i,j)=-Jg(i,j);
		  else
			 Jg(i,j)=-Jf(i,j)+(Pf(1,i)-Pi(1,i))./(Pi(1,2));
			 XJg(i,j)=-Jg(i,j);
		  end
	   end
    end
end

Jg(:,N+3)=-Jfreq;   
XJg(:,N+3)=Jfreq;

panpan=Pi(1,:)-Pf(1,:);		% (1,2N+2)

%to find a warning: w = warning('query','last')
warning('off','MATLAB:nearlySingularMatrix');
Delta=-inv(Jg)*panpan';	%
% (2N+2,1)

X=[];
X=Pi(1,:);
X(1,N+3)=FREQ(1);

bambi=X;
clear Pi;
Pi=bambi+Delta';
FREQ(2)=Pi(1,N+3);





