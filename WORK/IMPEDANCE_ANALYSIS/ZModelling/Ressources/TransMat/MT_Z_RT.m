function [Z, f, instru, Mguide, Zr, k] = MT_Z_RT(instru,f,Co,ro,Ta,nbTr,show)
%Author: Robin Tournemenne based on the Jean-Pierre Dalmont technique but using
%some formulas of Braden and Mapes-Riordan

k=(2*pi/Co)*f;
Ltot = 0;

%transfert matrices computation
for ii = 1:length(instru)
	switch lower(instru(ii).type)
		case 'cone'
			if ~isfield(instru(ii),'TransMat') || isempty(instru(ii).TransMat)% which is in fact the fixed transmission matrix
				if instru(ii).rayon(1)==instru(ii).rayon(2) %it is in fact a cylinder, then we avoid an error
					Mguide2 = MTcylindre_RT(instru(ii).longueur,...
						instru(ii).rayon(1),k,Ta,ro,Co);
				else
					Mguide2 = MTcone_RT(instru(ii).longueur,...
						instru(ii).rayon(1), instru(ii).rayon(2),...
						nbTr,k,Ta,ro,Co);
				end
				instru(ii).TransMat=Mguide2;
			else
				Mguide2=instru(ii).TransMat;
			end
		case 'cylindre'
			if ~isfield(instru(ii),'TransMat') || isempty(instru(ii).TransMat)
				Mguide2 = MTcylindre_RT(instru(ii).longueur,...
					instru(ii).rayon,k,Ta,ro,Co);
				instru(ii).TransMat=Mguide2;
			else
				Mguide2=instru(ii).TransMat;
			end
		otherwise
			error('type unknown');
    end
    Ltot = Ltot + instru(ii).longueur;
    if ii == 1
        Mguide = Mguide2;
    else
        Mguide = multmat(Mguide,Mguide2);
    end
end

Rend = instru(end).rayon(end);

Zr=zrayon_RT(k, Rend, ro, Co);
Z=impedanc(Mguide,Zr);

% R0 = instru(1).rayon(1);
% zc=ro * Co / (pi * R0^2);
% ze=Z/zc;

if show
    figure;
    subplot(221); plot(f,abs(Z),'k'); axis; title('Module Z');
    subplot(223); plot(f,angle(Z),'k'); axis; title('Phase Z');
    subplot(222); plot(f,real(Z),'k'); axis; title('Real(Z)');
    subplot(224); plot(f,imag(Z),'k'); axis; title('Imag(Z)');
end
