function y=multmat(matrice1,matrice2)
%Author: Jean-Pierre Dalmont after 1990

%I am pretty sure, nothing is fster that these operation for this task
N=length(matrice1)/4;
A=matrice1(1:N).*matrice2(1:N)+matrice1(N+1:2*N).*matrice2(2*N+1:3*N);
B=matrice1(1:N).*matrice2(N+1:2*N)+matrice1(N+1:2*N).*matrice2(3*N+1:4*N);
C=matrice1(2*N+1:3*N).*matrice2(1:N)+matrice1(3*N+1:4*N).*matrice2(2*N+1:3*N);
D=matrice1(2*N+1:3*N).*matrice2(N+1:2*N)+matrice1(3*N+1:4*N).*matrice2(3*N+1:4*N);
y=[A B C D];
end
