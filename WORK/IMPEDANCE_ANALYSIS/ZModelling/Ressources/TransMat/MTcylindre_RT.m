function matrice=MTcylindre_RT(l,r,k,Tc,ro,Co)

omega = k * Co;
s = pi*r*r;

Zc = ro * Co / s;
T0 = 273.15; %temperature absolue Kelvin
T = Tc+T0;
muu = 1.7141e-5*sqrt(T0/T);
rv=r*sqrt(ro * omega / muu);
Zcc = Zc * ((1+ 0.369 ./ rv) - 1i * 0.369 ./ rv);
G = k .* (1.045 ./ rv + 1i * (1 + 1.045 ./ rv));


a = cosh(G * l); 
b = Zcc .* sinh(G * l);
c = sinh(G * l) ./ Zcc;     
d = a;
matrice = [a, b, c, d];
