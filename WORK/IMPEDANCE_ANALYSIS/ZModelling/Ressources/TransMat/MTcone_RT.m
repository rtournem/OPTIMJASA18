function matrice = MTcone_RT(l,R0, R1, NbTr, k, Tc, ro, Co)

%% Calcul de la matrice de transmission
omega = k * Co;
T0 = 273.15; %temperature absolue Kelvin
T = Tc+T0;
muu = 1.7141e-5*sqrt(T0/T);
	
Rd=R0;
Rf=R1;
ltot=l;
l=l/NbTr;
for i=1:NbTr
	R0=interp1([0 ltot],[Rd Rf],(i-1)*l);
	R1=interp1([0 ltot],[Rd Rf],i*l,'linear',Rf);%Rf is the value is i*l extrapolate (see interp1 help)
		
	Rmoy = (R0 + R1) / 2;
	x0 = l * R0 / (R1 - R0);
	x1 = l * R1 / (R1 - R0);
	
	s0 = pi * R0^2;
	smoy = pi * Rmoy^2;
	

	rv = Rmoy * sqrt(ro * omega / muu);
	Zc = ro * Co / s0; %this come from the equation from our paper on the impedance
	
	Zcc = Zc * ((1+ 0.369 ./ rv) - 1i * 0.369 ./ rv);
	G = k .* (1.045 ./ rv + 1i * (1 + 1.045 ./ rv));
	
	a = x1 / x0 * cosh(G * l) - 1 ./ (G * x0) .* sinh(G * l);
	b = x0 / x1 * Zcc .* sinh(G * l);
	c = 1 ./ Zcc .* ((x1 / x0 - 1 ./ ((G * x0).^2)) .* sinh(G * l) + l ./ (G * x0^2) .* cosh(G * l));
	d = x0 / x1 * (cosh(G * l) + 1 ./ (G * x0) .* sinh(G * l));
	
	Mguide = [a, b, c, d];
	if i== 1
		matrice = Mguide;
	else
		matrice = multmat(matrice,Mguide);
	end
end
end
