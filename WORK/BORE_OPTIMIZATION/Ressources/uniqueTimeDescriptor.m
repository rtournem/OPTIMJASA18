function newTime = uniqueTimeDescriptor(oldTime, direction)
%oldTime is either the result of the function clock or the newTime calculated
%thanks to the direct part of this function

if strcmp(direction, 'direct')
	newTime = oldTime(6) + oldTime(5) * 10e2 + oldTime(4) * 10e4 + oldTime(3) *...
		10e6 + oldTime(2) * 10e8 + oldTime(1) * 10e10;
	
elseif strcmp(direction, 'inverse')
	newTime(1) = floor(oldTime / 10e10);
	oldTime = oldTime - newTime(1) * 10e10;
	newTime(2) = floor(oldTime / 10e8);
	oldTime = oldTime - newTime(2) * 10e8;
	newTime(3) = floor(oldTime / 10e6);
	oldTime = oldTime - newTime(3) * 10e6;
	newTime(4) = floor(oldTime / 10e4);
	oldTime = oldTime - newTime(4) * 10e4;
	newTime(5) = floor(oldTime / 10e2);
	newTime(6) = oldTime - newTime(5) * 10e2;
end
end
