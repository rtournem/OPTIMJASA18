function res=allcomb(a)
%cartesian product list
N = numel(a);
v = cell(N,1);
[v{:}] = ndgrid(a{:});
res = reshape(cat(N+1,v{:}), prod(cellfun(@length, a)), N);
end
