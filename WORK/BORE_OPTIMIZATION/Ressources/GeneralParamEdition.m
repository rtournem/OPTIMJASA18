function [MusicianNFO,PhysNFO,InstruNFO,SimuNFO,OptimNFO,impedNFO]=GeneralParamEdition(OPTProb,Candidate)
%provides a list of general structure to launch the Harmonic balance
%technique.
%Furthermore, it provides a definition of the parameters retrieving the
%impedance thnks to the optimization problem and the candidate value.

%-IMPED
Impedtype=OPTProb.Nature;

impedNFO=impedPreProcess(1,Impedtype);%1 because it is the step one of the imped parameterization
impedNFO=impedPreProcess(2,Impedtype,Candidate,impedNFO);
impedNFO.RegMax=8; %number of peaks we wanna know the frequency position

%-MUSICIAN
MusicianNFO.Ql=3;
MusicianNFO.H0=0.0001;
MusicianNFO.b=0.01;
%-PHYSICS
PhysNFO.Ta=20;
impedNFO.Ta=PhysNFO.Ta;
%-INSTRUMENT
InstruNFO.r=0.008;
impedNFO.r=InstruNFO.r;
%-SIMULATION
N=6;
SimuNFO.N=N;
%SimuNFO.fe=44100; %used to create sounds -> useless in bash
SimuNFO.ResEdit=0;
SimuNFO.Verbose=0; %1 if you want to see how the convergence is going
%-OPTIMSIMULATION
OptimNFO.strat={'OnlyFirstPartial','AllNeighbors_Depth_1','highBW'}; %OnlyFirstPartial   %FirstAndSecondPartial  %Closer2reality %AllNeighbors_Depth_X
OptimNFO.EPS=6;%puissance de 10
OptimNFO.Itermax=30; %useless to go to 600 we want to converge very rapidly, we don't want to bet on luck
end
