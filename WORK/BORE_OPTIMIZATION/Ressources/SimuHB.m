function [SelectedNotes,AllNotes,PowNotes]=SimuHB(OPTProb,Candidate,reg)
%manages the actual sound simulations
OPTProb.CurReg=reg;

%%                          INTERNAL PARAMS
SelectedNotes=[];
PowNotes=[];
AllNotes=[];
ForceSkip=0; %if too many vectors gives bad power then we skeep the research
%%                        Impedance definition
Masks=ones(1,3); %A non-sense value just to enter the while.
dispstat(['Candidate: ' num2str(Candidate)], 'keepthis');
dispstat(['regime: ' num2str(reg)], 'keepthis');
Cost=1;
while Masks
	%%                       Mask creation and initial vectors
	[Masks,OPTProb]=MaskManager(OPTProb,SelectedNotes,PowNotes,Candidate);
	if ~Masks %it is the end (leaving the while)
		break; %was continue before???
	end
	for i=1:size(Masks,1) %the first time we tried the right number of masks (then we fill it one by one)
		Mask=Masks(i,:);
		Pis=InitVectorOPTIM(OPTProb,Mask,Candidate,SelectedNotes,OPTProb.fPeaks,...
			OPTProb.holes,OPTProb.Optim.strat);

		for j=1:size(Pis,1)
			OPTProb.Simu.Pi=Pis(j,:);
			OPTProb.Mask.pm=Mask(1);
			OPTProb.Mask.mua=Mask(2);
			OPTProb.Mask.fa=Mask(3);
			try
				[IsGood,~,Pfinal]=...
					eqHarmo(OPTProb.Mask,OPTProb.Instru,OPTProb.Simu,OPTProb.Phys,OPTProb.Optim);
			catch ME
				disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
				disp('erreur EqHarmo');
				disp(ME.message);
				disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
				IsGood=0;
			end
			if IsGood
				[SelectedNotes,ForceSkip,STOP,AllNotes,PowNotes]=NoteValidation(OPTProb,...
					Mask,Pfinal,SelectedNotes,OPTProb.holes,ForceSkip,AllNotes,PowNotes);
				if STOP
					if Pis(j,5)==0
					end
					break;
				end
			end
		end
		Cost=Cost+1;
	end
	if Cost>10*OPTProb.PmZone %to avoid bad geometry still taken into account.
		disp(['COST REACHED, ' num2str(size(SelectedNotes,1)) ' sounds were sadly found...']);
		return;
	end
end
end
