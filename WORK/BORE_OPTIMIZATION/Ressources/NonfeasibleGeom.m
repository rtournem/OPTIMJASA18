function STOP=NonfeasibleGeom(OPTProb,Candidate,toPlot)
%test against the preprocessing if the incumbet instrument geometry is playable.
%discard it if not possible

InitSols = OPTProb.INITSOLS;
Pms=OPTProb.desc{:,2};
for Pm=Pms'
	idxPm = cellfun(@(x) isequal(x, Pm'), OPTProb.PMS, 'UniformOutput', 1);
	for reg=OPTProb.reg
		%We have to take all the geometries of the regime around ALL the Pms we have chosen!!!!
		%+ and - 500 Pa because it doesn't make such a difference for the zone circonscription.
		idxReg= InitSols(:,end-length(Candidate))==reg & InitSols(:,1)>=Pm(1)-500 & InitSols(:,1)<=Pm(2)+500;
		GeomsReg=InitSols(idxReg,[end-length(Candidate)+1:end,1]);
		if toPlot
			figure('Name',num2str(reg));
			plot(GeomsReg(:,1),GeomsReg(:,2),'.');
			text(GeomsReg(:,1),GeomsReg(:,2),num2str(GeomsReg(:,3)));
			hold on;
			plot(Candidate(1),Candidate(2),'*r');
		end

		minVal = repmat(OPTProb.minVal{idxPm,reg}(4:end), size(GeomsReg, 1), 1);
		maxVal = repmat(OPTProb.maxVal{idxPm,reg}(4:end), size(GeomsReg, 1), 1);
		paramNorm = (GeomsReg(:,1 : end - 1) - minVal) ./ (maxVal - minVal);
		CandidateNorm = (Candidate - minVal(1,:)) ./ (maxVal(1,:) - minVal(1,:));
		distNeigh = sort(pdist2(paramNorm, CandidateNorm, 'chebychev'));
		if distNeigh(OPTProb.neighborIdx) < OPTProb.neighborsDesign{idxPm}(reg)
			STOP=0;
		else
			STOP=1; break;
		end
	end
	if STOP
		break;
	end
end
end
