function [fig, FontSize, FontSizeTicks, namefig, figBW] = kmeansplot(fig2plot)
switch fig2plot
	case 12
		figdata = 'kmeans_intonation.fig';
		dataNb = 6;
		namefig = 'kmeansintonation';
		txt = '\textbf{Int}';
	case 22
		figdata = 'kmeans_scaverage.fig';
		dataNb = 3;
		namefig = 'kmeansSCaverage';
		txt = '\textbf{SC - Int}';
	case 32
		figdata = 'kmeans_scdynamics.fig';
		dataNb = 7;
		namefig = 'kmeansSCdynamics';
		txt = '\textbf{SC Dyn}';
end
FontSize = 60;
FontSizeTicks = 45;
LW = 2;
open(figdata);
hh = get(gcf,'children');
ax2 = hh(2);
ll = get(ax2,'Children');
dataObj = ll(end - dataNb + 1: end);
for i = 1 : dataNb
	data(i,:) = dataObj(i).YData;
end
data = [ones(dataNb, 1) .* 4.64 , data .* 1e3, ones(dataNb, 1) .* 5.825];
close all
axialpos = ((9 : 2 : 31) + 0.3267) * 1e-2;
pos = [200, 200, 1100, 700];

%1 color, 2 BW
for i = 1:2
	
	figStyl(i) = figure(	'position', pos);
	hold on
	if i==1
		colors = copper(size(data, 1));
	else
		colors = gray(size(data, 1)) .* 0.7;
	end
	for i = 1 : size(data, 1)
		plot(axialpos, data(i,:), 'linewidth', LW, 'color', colors(i, :));
	end
	if fig2plot ~=22 && fig2plot ~=32
		ylabel('Inner shape radius (mm)');
	end
	xlim([0.093267, 0.313267]);
	ylim([0 10]);
	text(0.25, 8.5, txt, 'Interpreter', 'Latex', 'Fontsize', FontSize);
	xlabel('Axial position (m)');
	
end
fig = figStyl(1);
figBW = figStyl(2);

end
