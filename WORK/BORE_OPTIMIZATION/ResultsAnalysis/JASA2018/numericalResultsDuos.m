clear all
close all
ROOT = '../../../../';
cs = reshape(1 : 12, 2, 6)';
idx = 6;

load(['result_' num2str(cs(idx, 1))]);
Cdts = bestcandidate;
initBbos = bBBOs(:, 1);
bbos = bBBOs(:, end);
bestEnsemble = mean(bbos);
initEnsemble = mean(initBbos);
averageImproveEnsemble = abs(bestEnsemble - initEnsemble);

load(['result_' num2str(cs(idx, 2))]);
Cdts = [Cdts; bestcandidate];
bbos = [bbos; bBBOs(:, end)];
initBbos = bBBOs(:, 1);
bestLOWESS = mean(bBBOs(:, end));
initLOWESS = mean(initBbos);
averageImproveLOWESS = abs(bestLOWESS - initLOWESS);

switch idx
	case {1, 4}
		[valB, idxB] = min(bbos);
		CdtsB = Cdts(idxB, : );
	case {2, 3, 5, 6}
		[valB, idxB] = max(bbos);
		CdtsB = Cdts(idxB, : );
		averageImproveEnsemble = averageImproveEnsemble / 5 * 100;
		averageImproveLOWESS = averageImproveLOWESS / 5 * 100;
end

disp(['Bestrun idx ' num2str(idx) ' val: ' num2str(valB)]);
disp(['Bestrun idx ' num2str(idx) ' Candidat:']);
disp( num2str(CdtsB * 10^3));
disp('--------------------------------------');
disp(['average improvement Ensemble idx '  num2str(idx) ' val: ' num2str(averageImproveEnsemble)]);
disp(['average improvement LOWESS idx '  num2str(idx) ' val: ' num2str(averageImproveLOWESS)]);

