ROOT = '../../../../';
path(path, [ROOT 'REDUNDANT']);

N = 6;
for i = 1 : 20
data = ['~/REPO/THESE/DATA/musician/branches_embouchure_2006/crescendo/sib-DKOS_' num2str(i) '.wav'];
disp('---------------');
disp(num2str(i));
[s,Fe]=audioread(data);
%la fft du signal r�el
L=length(s);
NFFT = 2^nextpow2(L); % Next power of 2 from length of y
Y = fft(s,NFFT)/L;
f = Fe/2*linspace(0,1,NFFT/2+1);
% FftPlt=figure;
% plot(f,2*abs(Y(1:NFFT/2+1)));
% title('FFT');
% xlabel('frequency (Hz)');
% ylabel('|FFT|');

[peaksfr,peaksvl,Ythresh]=PeakDetection(Y(1:NFFT/2+1),N,200,f);

%harmo de la synth�se
% figure(FftPlt);
% hold on;
% plot(f([1;end]),2.*[Ythresh;Ythresh],'r');
% plot(peaksfr,2*abs(peaksvl),'*r');

CGS(i)=abs(peaksvl)*(1:N)'./sum(abs(peaksvl));
disp(num2str(CGS(i)));
% keyboard;
% close all
end
figure;
plot(CGS);

disp(['cgs piano: ' num2str(mean(CGS(1:4)))]);
disp(['cgs ff: ' num2str(mean(CGS(16:20)))]);
disp(['diff: ' num2str(mean(CGS(16:20))-mean(CGS(1:4)))]);
disp(['diff %: ' num2str((mean(CGS(16:20))-mean(CGS(1:4)))/5)]);


