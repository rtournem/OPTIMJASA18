function gray = c2g(color)
%algo luminosity qui est obligatoiremeent celui utilis� par l'imprimante sinon
%�a n'a pas de sens d'avoir une dif�rence entre lebleu et le rouge.
gray = color * [0.21, 0.72, 0.07]';

% algo average
% gray = color * [0.33, 0.33, 0.33]';
gray = [gray, gray, gray];
end
