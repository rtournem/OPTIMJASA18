function [tri, crossingTri, x1LinClean, x2LinClean, toPlotLinClean] =...
	dataSurface4Plot(designSpace, toPlot)
x1Lin=repmat(designSpace{1}',length(designSpace{2}),1);
x2Lin=repmat(designSpace{2},length(designSpace{1}),1);
x2Lin=x2Lin(:);%c'est pareil que x1Lin mais en 2 lignes
faceColor={[0 0.9 0],[0.6875 0.875 0.8984],[0.6875 0.875 0.8984]};
faceAlpha={0.7,0.3,0.3};
edgeColor={[0 1 0],[0.5875 0.775 0.7984],[0.5875 0.775 0.7984]};
hold on;
% for i=1
toPlotLin=toPlot(:);
toPlotLin(isinf(toPlotLin))=0;
x1LinClean=x1Lin(toPlotLin>0);
x2LinClean=x2Lin(toPlotLin>0);
toPlotLinClean=toPlotLin(toPlotLin>0);
tri=delaunay(x1LinClean,x2LinClean);
crossingTri = intersectionplaneDelaunay(tri, toPlotLinClean, 12.26);
end
