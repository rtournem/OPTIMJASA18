function PLOTCGSPm(Candidates,BBO,OPTProb,info)

figure;
for i=1:length(info) %selon les candidats
	if BBO(i)
		BBOreg(i,:)=info{i}{1}{1};
	end
end
for reg=OPTProb.reg
	subplot(2,3,reg); 
	Delaunayplotting(Candidates,{BBOreg(:,reg)});
end
figure;
Delaunayplotting(Candidates,{BBO});

end
function data = Delaunayplotting(designSpace,toPlot)
x1Lin=repmat(designSpace{1}',length(designSpace{2}),1);
x2Lin=repmat(designSpace{2},length(designSpace{1}),1);
x2Lin=x2Lin(:);
faceAlpha={0.7,0.3,0.3};
hold on;
toPlotLin=toPlot{1}(:);
toPlotLin(isinf(toPlotLin))=0;
x1LinClean=x1Lin(toPlotLin>0);
x2LinClean=x2Lin(toPlotLin>0);
toPlotLinClean=toPlotLin(toPlotLin>0);
tri=delaunay(x1LinClean,x2LinClean);
crossingTri = intersectionplaneDelaunay(tri, toPlotLinClean, 12);
trisurf(tri,x1LinClean,x2LinClean,toPlotLinClean,'FaceAlpha',faceAlpha{1}, 'FaceColor', 'interp');
data{1}.tri = tri;
data{1}.crossingTri = crossingTri;
data{1}.x1LinClean = x1LinClean;
data{1}.x2LinClean = x2LinClean;
data{1}.toPlotLinClean = toPlotLinClean;
cameramenu;
end

function [crossingTri, idxCrossingTri] = intersectionplaneDelaunay(tri, pointVal, planeHeight)

triVal = arrayfun(@(x) pointVal(x), tri);
triStatus = arrayfun(@(x) x > planeHeight, triVal);
triStatus = sum(triStatus, 2);
%au dessus du plan
idxCrossingTri = find(triStatus == 3);
%ligne
% idxCrossingTri = find(triStatus > 0 & triStatus < 3);
crossingTri = tri(idxCrossingTri, :);

end
