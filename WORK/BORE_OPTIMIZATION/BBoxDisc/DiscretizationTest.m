%this script let you compute and draw for a 2D design problem, the objective
%function you want to study.

%% cleaning
clear variables;
close all hidden;
global ROOT
ROOT='../../../';
path(path,genpath([ROOT 'WORK/FREQUENTIAL_SIMULATION']));
path(path,genpath([ROOT 'WORK/BORE_OPTIMIZATION/Ressources']));
path(path,genpath([ROOT 'WORK/BORE_OPTIMIZATION/BBoxDisc/Ressources']));
path(path,genpath([ROOT 'WORK/IMPEDANCE_ANALYSIS']));
warning('off','MATLAB:MKDIR:DirectoryExists');

%%
%you chose the design criterion to study, the design problem and the name of the
%catalyst files.  
Problem.crit = 'inharmpm';%inharmpm cgspm cgsplasticity
Problem.name = 'MP2TMM';
Problem.catalyst = 'MP2TMM_3idxPM';

%grid of points you want to evaluate
Candidates={0 : 0.0005 : 0.0025, 0.0014 : 0.0002 : 0.0022}; %coarse
% Candidates={0 : 0.00003 : 0.0025, 0.0014 : 0.00001 : 0.0022}; %very fine 
% Candidates={0 : 0.00012 : 0.0025, 0.0014 : 0.00004 : 0.0022}; %fine

%you can bypass the simulation process to load directly a file and/or even bypass
%the computation of the criterion for the representation.
bypassCrit = 0;
bypassSimu = 0;
if bypassCrit | bypassSimu 
	%you have to place the name of the stamp at the end of the file you want to
	%load
	Stamp = '';
else
	time = clock;  Stamp = uniqueTimeDescriptor(time, 'direct');
	%randomize everything
	rng(mod(Stamp, 2^32));
	tic;
end
%%                        Problem Definition
OPTProb=OPTProbDef(Problem);
OPTProb.ROOT=ROOT;
dataPath = [ROOT 'DATA/optim/discretisation/' Problem.catalyst '/' num2str(floor(Stamp))];
mkdir(dataPath); mkdir([dataPath '/results']); mkdir([dataPath '/notes']);

xspacePoints=allcomb(Candidates)';
if bypassCrit
	% 		load([dataPath '/results/result_' Problem.name]);
else
	BBO=zeros(size(xspacePoints,2),1);
	info=cell(size(xspacePoints,2),1);
	idx=1;
	for idxX=1:size(xspacePoints,2)
		Candidate=xspacePoints(:,idxX)';
		disp(['Candidate: ' num2str(Candidate)]);
		%if we already processed the notes
		if bypassSimu
			% 			load([dataPath '/notes/notes_' Problem.name '_' strrep(sprintf('%f_%f', Candidate(1),...
			% 				Candidate(2)), '.', '_')]);
			% 			OPTProb=OPTProbDef(Problem);
		else
			%%                        Perform simulation
			try
				[Notes,Pow,AllNotes]=OutputSimu(OPTProb,Candidate);
			catch ME %a random error in the code or initvect or mask or resonator error
				disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
				disp('unknown error in OutputSimu');
				disp(ME.identifier);
				disp('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!');
				Notes=cell(OPTProb.SimuSize(1),OPTProb.SimuSize(2));
				Pow=cell(OPTProb.SimuSize(1),OPTProb.SimuSize(2));
				AllNotes=cell(OPTProb.SimuSize(1),OPTProb.SimuSize(2));
			end
			save([dataPath '/notes/notes_' Problem.name '_' strrep(sprintf('%f_%f', Candidate(1),...
				Candidate(2)), '.', '_')], 'Notes', 'AllNotes', 'Pow', 'Candidate', 'OPTProb');
			toc;
		end
		%%                      Compute aggregated obj
		try %si on a pas eu assez de conv alors fail et on doit catch
			[BBO(idx),~,info{idx}]=CritCalc(OPTProb,Notes);
		catch
			BBO(idx)=0;info{idx}=0;
		end
		idx=idx+1;
		disp(['---------' num2str(100 * idxX / size(xspacePoints, 2)) '% candidates done']);
	end
	% 	save([dataPath '/results/result_CGSPlasticity' Problem.name],...
	% 		'BBO','Uncertainty','info','OPTProb','Candidates');
end

%% drawings
switch Problem.crit
	case 'inharmpm'
		PlotInharmpm(Candidates,BBO,OPTProb,info)
	case 'cgspm'
		PLOTCGSPm(Candidates,BBO,OPTProb,info);		
	case 'cgsplasticity'
		PLOTCGSPm(Candidates,BBO,OPTProb,info);
end
