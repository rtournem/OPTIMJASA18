#########################
#    2D   SeOe          #
#########################

DIMENSION 2
x0        x.txt
BB_EXE    'matlab_server.sh $bb'
BB_OUTPUT_TYPE OBJ
upper_bound ( 0.0025 0.0022 )
lower_bound ( 0 0.0014 )
MAX_BB_EVAL 100

#====================================
# GENERAL DISPLAY INFO
#====================================
DISPLAY_STATS BBE OBJ
DISPLAY_DEGREE 1
DISPLAY_ALL_EVAL yes

#====================================
# GENERAL MADS INFO
#====================================
DIRECTION_TYPE ORTHO N+1 NEG
SPECULATIVE_SEARCH yes
MODEL_SEARCH_PROJ_TO_MESH true
SEED DIFF

#====================================
# GENERAL INFO FOR SGTELIB
# (Does not impact MADS if sgtelib is not used)
#====================================
SGTELIB_MODEL_EVAL_NB 1000
SGTELIB_MODEL_FEASIBILITY C
SGTELIB_MODEL_FORMULATION FS
SGTELIB_MODEL_DISPLAY SOIP


#====================================
# DEFINE WHICH MODEL IS USED
#====================================
MODEL_SEARCH SGTELIB
MODEL_EVAL_SORT SGTELIB
SGTELIB_MODEL_DEFINITION TYPE ENSEMBLE WEIGHT SELECT METRIC OECV

#====================================
# OUTPUT FILE
#====================================
STATS_FILE Results.txt BBE ( SOL ) OBJ



