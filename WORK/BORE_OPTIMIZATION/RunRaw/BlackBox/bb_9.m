function BBO = bb(Candidate)
%10D design problem, studying the cgs under inharmonicity constraint, using the
%Ensemble surrogate 

% if you want to test this function uncomment Candidate and run it
% Candidate=[0 0.00185];
format long
disp(Candidate);
format short
if size(Candidate, 1) > 1
    error('x must be a row vector and must be a single point of the design space');
end
warning off; %#ok<WNOFF>
global ROOT
%This may need some adjustment depending on your architecture
if exist([pwd '/../BlackBox'], 'dir') %if you launch this function in its original folder to test
	ROOT = '../../../../';
else %if you launch this function in a job using optim.sh
	%it is almost sure you will have to change this ROOT path
	%look at the output of optim.sh to understand if it is right or not.
	if ismac
		ROOT = '../../../../../../THESE/'; %this was my personal setup...
	else
		ROOT = '../../../../../';
	end
end
path(path, genpath([ROOT 'WORK/FREQUENTIAL_SIMULATION']));
path(path, genpath([ROOT 'WORK/BORE_OPTIMIZATION/Ressources']));
path(path, genpath([ROOT 'WORK/IMPEDANCE_ANALYSIS']));
warning('off', 'MATLAB:MKDIR:DirectoryExists');
time = clock;  oneNumberTime = uniqueTimeDescriptor(time, 'direct');
rng(mod(oneNumberTime, 2^32)); %to shuffle everything everytime
%%          Problem Definition
Problem.crit = 'inharmpm';%inharmpm cgspm cgsplasticity
Problem.crit = 'cgspmunderinharm';%inharmpm cgspm cgsplasticity
Problem.name = 'LP10';
Problem.catalyst = 'LP10_3idxPM';
OPTProb = OPTProbDef(Problem);
%%      Perform simulation
psi = OutputSimu(OPTProb, Candidate);
%%      Compute aggregated obj
try
	BBO = CritCalc(OPTProb, psi);
catch
	BBO = NaN;
end
save(['Results/' strrep(num2str(oneNumberTime), '.', '') '.mat' ],...
	'Candidate', 'Problem', 'OPTProb', 'psi', 'BBO');

disp(['resultat: ' num2str(BBO) ' ;']);
disp('end');
end
