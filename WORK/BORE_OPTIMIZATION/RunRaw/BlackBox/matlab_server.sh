#!/bin/bash

# log file 
logfile='tmp/matlab_server.log'
# radical of all the status files
rad='tmp/matlab_server.status'

rm -f $rad.* 2>/dev/null

echo "***matlab_server.sh***" > $logfile

# Create instruction file for matlab_server
echo "Create status.new" >> $logfile
echo "cmd:" $@ >> $logfile
echo $@ > $rad.tmp
cp $rad.tmp $rad.new

# Wait for Matlab confirmation
# that the calculation has been started
counter=50000
while [ ! -f $rad.started ] && [ ! -f $rad.finished ]
do
  sleep 0.0001
  let counter--
  if [[ "$counter" -lt 0 ]]; then
    echo "matlab_server.sh did not receive the status \"started\" from matlab_server.m. EXIT !" >> $logfile
    exit 1
  fi
done
echo "Received status.started" >> $logfile

# Wait for Matlab signal that the calculation is over
while [ ! -e $rad.finished ]
do 
  sleep 0.0001
done

echo "Received status.finished" >> $logfile

# Display output for nomad
echo "Output:" >> $logfile
cat $rad.finished >> $logfile
cat $rad.finished

# Remove all status files
rm $rad.*



