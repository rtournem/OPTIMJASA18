#!/bin/bash

#SBATCH -J optims
#SBATCH -N 3
#SBATCH -n 3
#SBATCH --cpus-per-task=24
#SBATCH -p longq
#SBATCH -t 3-00:00:00

#ce script a une ooption qui est le cas étudié

# 3 by 3

for idx in {0..2}
do
    for paraidx in {1..3}
    do
        srun -N1 -n1 --cpus-per-task=24 hd run -n "optim cas $1" ./optim.sh $1 $(($paraidx+$idx*3)) &
    done
    wait
done
