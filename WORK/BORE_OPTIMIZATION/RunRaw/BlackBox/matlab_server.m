close all
clear all

%profile on

disp('Start Matlab server');

disp('Clean status files');
%Edit Robin 11/06/2018: je ne comprends pas pourquoi je n'arrive
%pas a démarrer les optim parfois car le fichier tmp disparait
! rm tmp/matlab_server.status.* 2>/dev/null


wait_tmax = 1000;
wait_dt = 0.001;
wait_t = 0;

DISPLAY = true;

cmd_number = 0;

while wait_t < wait_tmax
	
	pause(wait_dt);
	wait_t = wait_t+wait_dt;
	
	% Check if there is a new input file
	if exist([pwd '/tmp/matlab_server.status.new'],'file')
		if DISPLAY
			disp('Detected: tmp/matlab_server.status.new');
		end
		
		
		s = importdata('tmp/matlab_server.status.new');
		s = s{1};
		i = find(isspace(s));
		fcn = s(1:i-1); %The blackbox matlab function is retrieved
		xfile = s(i+1:end);
		
		
		if DISPLAY
			disp('status.new --> status.started');
		end
		! mv tmp/matlab_server.status.new tmp/matlab_server.status.started
		
		output = [];
		cmd = ['output = ' fcn '(x);']; %old way to make eval return the results in output
		cmd_number = cmd_number+1;
		if true
			disp(['cmd #' num2str(cmd_number) '  :  "' cmd '"']);
		end
		try
			% Read x file
			x = importdata(xfile);
			% Eval cmd
			eval(cmd);
		catch me
			disp(me.message);
			output = 99e+99*ones(1,100);
		end
		
		if DISPLAY
			disp('Write output in tmp/matlab_server.status.started');
		end
		fid = fopen('tmp/matlab_server.status.started','w');
		fwrite(fid,num2str(output,16));
		fclose(fid);
		if DISPLAY
			disp('status.started --> status.finished');
		end
		! mv tmp/matlab_server.status.started tmp/matlab_server.status.finished
		
		% Reset timer
		wait_t = 0;
		if DISPLAY
			disp('Back to waiting...');
		end
	end
	
	if exist([pwd 'tmp/matlab_server.status.quit'],'file')
		quit;
	end
	
	%if cmd_number>200
	%  break;
	%end
	
end

%profile viewer

disp('Time limit exceeded');

disp('Clean status and exchange files');
! rm tmp/matlab_server.status.* 2>/dev/null

